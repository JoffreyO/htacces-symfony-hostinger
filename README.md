# Htacces Symfony Hostinger


Use this file to configure your Symfony Project on Hostinger host.

## Getting started

You need to put this .htaccess to your project root.

And after that, use this command to create automatic .htaccess in your /public folder :

```
php composer.phar require symfony/apache-pack
```

And Voila ! Your website is working.